/**
 * @file    ReflectedSerial.cpp
 * @brief   Software Buffer - Extends mbed Serial functionallity adding irq driven TX and RX
 * @author  sam grove
 * @version 1.0
 * @see
 *
 * Copyright (c) 2013
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include "ReflectedSerial.h"
#include <stdarg.h>

ReflectedSerial::ReflectedSerial(PinName tx, PinName rx, uint32_t baud, uint32_t buf_size, uint32_t tx_multiple, const char* name)
    : RawSerial(tx, rx, baud) , _rxbuf(buf_size), _txbuf((uint32_t)(tx_multiple*buf_size)), _forward(NULL)
{
    // attach(callback(obj, method), type)
    RawSerial::attach(callback(this, &ReflectedSerial::rxIrq), Serial::RxIrq);
    this->_buf_size = buf_size;
    this->_tx_multiple = tx_multiple;   
    return;
}

ReflectedSerial::~ReflectedSerial(void)
{
    RawSerial::attach(NULL, RawSerial::RxIrq);
    RawSerial::attach(NULL, RawSerial::TxIrq);

    return;
}

void ReflectedSerial::forward_rx(RawSerial *_ser)
{
    this->_forward = _ser;
}

int ReflectedSerial::readable(void)
{
    return _rxbuf.available();  // note: look if things are in the buffer
}

int ReflectedSerial::writeable(void)
{
    return 1;   // buffer allows overwriting by design, always true
}

int ReflectedSerial::getc(void)
{
    return _rxbuf;
}

int ReflectedSerial::putc(int c)
{
    _txbuf = (char)c;
    ReflectedSerial::prime();

    return c;
}

int ReflectedSerial::puts(const char *s)
{
    if (s != NULL) {
        const char* ptr = s;
    
        while(*(ptr) != 0) {
            _txbuf = *(ptr++);
        }
        _txbuf = '\n';  // done per puts definition
        ReflectedSerial::prime();
    
        return (ptr - s) + 1;
    }
    return 0;
}

int ReflectedSerial::printf(const char* format, ...)
{
    char buffer[this->_buf_size];
    memset(buffer,0,this->_buf_size);
    int r = 0;

    va_list arg;
    va_start(arg, format);
    r = vsprintf(buffer, format, arg);
    // this may not hit the heap but should alert the user anyways
    if(r > this->_buf_size) {
        error("%s %d buffer overwrite (max_buf_size: %d exceeded: %d)!\r\n", __FILE__, __LINE__,this->_buf_size,r);
        va_end(arg);
        return 0;
    }
    va_end(arg);
    r = ReflectedSerial::write(buffer, r);

    return r;
}

ssize_t ReflectedSerial::write(const void *s, size_t length)
{
    if (s != NULL && length > 0) {
        const char* ptr = (const char*)s;
        const char* end = ptr + length;
    
        while (ptr != end) {
            _txbuf = *(ptr++);
        }
        ReflectedSerial::prime();
    
        return ptr - (const char*)s;
    }
    return 0;
}


void ReflectedSerial::rxIrq(void)
{
    // read from the peripheral and make sure something is available
    if(serial_readable(&_serial)) {
        char c = serial_getc(&_serial);
        _rxbuf = c; // if so load them into a buffer
        if (_forward != NULL) {
            _forward->putc(c);
        }
    }

    return;
}

void ReflectedSerial::txIrq(void)
{
    // see if there is room in the hardware fifo and if something is in the software fifo
    while(serial_writable(&_serial)) {
        if(_txbuf.available()) {
            serial_putc(&_serial, (int)_txbuf.get());
        } else {
            // disable the TX interrupt when there is nothing left to send
            RawSerial::attach(NULL, RawSerial::TxIrq);
            break;
        }
    }

    return;
}

void ReflectedSerial::prime(void)
{
    // if already busy then the irq will pick this up
    if(serial_writable(&_serial)) {
        RawSerial::attach(NULL, RawSerial::TxIrq);    // make sure not to cause contention in the irq
        ReflectedSerial::txIrq();                // only write to hardware in one place
        RawSerial::attach(callback(this, &ReflectedSerial::txIrq), RawSerial::TxIrq);
    }

    return;
}


